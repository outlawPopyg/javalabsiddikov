# Вложенные классы
* Вложенный класс - это класс, определенный внутри другой области (класс, метод)

## Статически вложенный класс (просто вложенный)
* Объявляется с модификатором `static`
* Просто один код внутри другого
* Обрамляющий класс (внешний класс) имеет доступ ко всем членам вложенного  
* Ассоциирован с внешним классом, следовательно, не имеет доступ к нестатическим членам класса

**Пример**:
```java
public class LinkedList {
    private Node first;
    private Node last;

    private int count;

    public void add(int value) {
        Node newNode = new Node(value);

        if (count == 0) {
            this.first = newNode;
        } else {
            this.last.next = newNode;
        }

        this.last = newNode;
        count ++;
    }

    public int get(int index) {
        Node current = this.first;
        if (index >= 0 && index < count) {
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        } else throw new IndexOutOfBoundsException();
    }

    public static class Node {
        private final int value;
        private Node next;

        public Node(int value) {
            this.value = value;
        }

    }
}
```

## Внутренний класс (inner)
* Объявляется без модификатора `static`
* Ассоциирован с объетком внешнего класса
* Имеет доступ ко **всем** членам внешнего класса

**Пример:**
```java
package com.outlaw.collections;

public class LinkedList implements Iterable {
    Node first;
    Node last;

    int count;

    public void add(int value) {
        Node newNode = new Node(value);

        if (count == 0) {
            this.first = newNode;
        } else {
            this.last.next = newNode;
        }

        this.last = newNode;
        count ++;
    }

    public int get(int index) {
        Node current = this.first;
        if (index >= 0 && index < count) {
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        } else throw new IndexOutOfBoundsException();
    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }

    static class Node {
        final int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }

    }

    // inner class
    public class LinkedListIterator implements Iterator {
        private Node current;

        public LinkedListIterator() {
            this.current = first;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public int next() {
            int value = current.value;
            this.current = this.current.next;

            return value;
        }
    }
}

```

```text
    Создать экземпляр LinkedListIterator можно только через уже существующий объект LinkedList, поскольку LinkedListIterator связан полем first класса LinkedList  
    LinkedList.LinkedListIterator listIterator = list.new LinkedListIterator();
```

![](./img.png)
