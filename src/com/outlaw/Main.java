package com.outlaw;

import com.outlaw.collections.Iterator;
import com.outlaw.collections.LinkedList;

public class Main {
    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();
        list.add(13);
        list.add(124);
        list.add(1);

        Iterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

//        LinkedList.LinkedListIterator listIterator = list.new LinkedListIterator();
    }
}
