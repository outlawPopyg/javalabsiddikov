package com.outlaw.collections;

public interface Iterable {
    Iterator iterator();
}
