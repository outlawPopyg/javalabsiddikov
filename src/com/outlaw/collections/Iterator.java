package com.outlaw.collections;

public interface Iterator {
    boolean hasNext();
    Object next();
}
